import asyncio
import client as websocket_client
import rfid_reader
import json


async def app_loop():
    client = websocket_client.Client()
    if not await client.connect():
        return

    reader = rfid_reader.RfidReader()
    read = reader.read()

    ping_timeout = 0

    try:
        while True:
            status, data = next(read)
            if status > rfid_reader.RfidReader.STATUS_NONE:
                ping_timeout = 0

                if status == rfid_reader.RfidReader.STATUS_NEW:
                    await client.send("rfid-new-figure", data)
                else:
                    await client.send("rfid-request-update", "")
                    updates = await client.read()
                    if updates.action == "rfid-submit-update" and updates.message != "":
                        reader.write(json.dumps(updates.message))
            else:
                if ping_timeout > 30:
                    ping_timeout = 0
                    await client.send("ping", "")
                    await client.read()
                ping_timeout += 1
    except KeyboardInterrupt:
        reader.close()


asyncio.get_event_loop().run_until_complete(app_loop())
