import websockets
import json
import dto


class Client:
    websocket: websockets.WebSocketClientProtocol

    async def connect(self) -> bool:
        uri = "ws://localhost:8765"
        self.websocket = await websockets.connect(uri)
        await self.send("connection-request", "rfid-reader")

        response = await self.read()
        if response.action == "connection-approval" and response.message == "True":
            return True
        return False

    async def send(self, action: str, message: str):
        await self.websocket.send(json.dumps(dto.Dto(action, message).__dict__()))
        print(f"> {action} {message}")
        return

    async def read(self) -> dto.Dto:
        response = dto.Dto.__from_dict__(json.loads(await self.websocket.recv()))
        print(f"< {response.action} {response.message}")
        return response

