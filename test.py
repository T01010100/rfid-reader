
import RPi.GPIO as GPIO
import SimpleMFRC522 as mfrc

reader = mfrc.SimpleMFRC522()

i, t = reader.read()
print(f"read - {i}: {t}")

data = input()
i, t = reader.write(data)
print(f"write - {i}: {t}")

i, t = reader.read()
print(f"read - {i}: {t}")

GPIO.cleanup()
