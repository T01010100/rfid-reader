import asyncio
import websockets
import json
import dto

# SERVER Clients
rfid_reader = set()
web_apps = set()

clients = {
    "rfid-reader": rfid_reader,
    "web-app": web_apps
}

write_queue = ""


# SERVER Actions
async def invalid_action(websocket: websockets.WebSocketServerProtocol, _: str) -> bool:
    await send(websocket, "error", "invalid action")
    return False


async def handle_ping(websocket: websockets.WebSocketServerProtocol, value: str) -> bool:
    await send(websocket, "ping", "")
    return True


async def handle_connection_request(websocket: websockets.WebSocketServerProtocol, value: str) -> bool:
    if value not in clients:
        await send(websocket, "connection-approval", "False")
        return False
    websocket_set = clients.get(value)
    websocket_set.add(websocket)

    await send(websocket, "connection-approval", "True")
    return True


async def handle_rfid_new_figure(websocket: websockets.WebSocketServerProtocol, value: str) -> bool:
    for ws in web_apps:
        pass
        #await send(ws, "rfid-new-figure", value)
    return True


async def handle_rfid_request_update(websocket: websockets.WebSocketServerProtocol, value: str) -> bool:
    global write_queue

    data = write_queue
    if data and data != "":
        await send(websocket, "rfid-submit-update", data)
        write_queue = ""

    await send(websocket, "rfid-submit-update", "")
    return True


async def handle_figure_update(websocket: websockets.WebSocketServerProtocol, value: str) -> bool:
    global write_queue

    write_queue = value
    return True


actions = {
    "connection-request": handle_connection_request,
    "ping": handle_ping,
    "rfid-new-figure": handle_rfid_new_figure,
    "rfid-request-update": handle_rfid_request_update,
    "update-figure": handle_figure_update
}


# SERVER Websockets
async def send(websocket: websockets.WebSocketServerProtocol, action: str, message: str):
    print(f"> {action} {message}")
    await websocket.send(json.dumps(dto.Dto(action, message).__dict__()))


async def read(websocket: websockets.WebSocketServerProtocol) -> dto.Dto:
    response = dto.Dto.__from_dict__(json.loads(await websocket.recv()))
    print(f"< {response.action} {response.message}")
    return response


async def handle(websocket: websockets.WebSocketServerProtocol, path):
    connected = True
    print(f":: websocket connected")

    while connected:
        try:
            data = await read(websocket)

            action = actions.get(data.action, invalid_action)
            await action(websocket, data.message)
        except:
            connected = False

    if websocket in web_apps:
        web_apps.remove(websocket)
    if websocket in rfid_reader:
        rfid_reader.remove(websocket)

    print(f":: websocket disconnected")


print("websocket server started")

start_server = websockets.serve(handle, "localhost", 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
