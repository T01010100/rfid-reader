

class Dto:
    action: str
    message: str

    def __init__(self, action: str, message: str):
        self.action = action
        self.message = message

    def __dict__(self):
        return {
            "action": self.action,
            "message": self.message
        }

    @staticmethod
    def __from_dict__(data: dict):
        return Dto(data["action"], data["message"])
