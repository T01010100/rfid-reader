from time import sleep

import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522


class RfidReader:
    STATUS_NONE = 0
    STATUS_NEW = 1
    STATUS_SAME = 2

    reader: SimpleMFRC522
    exit: bool

    queue_data: str
    write_request: bool

    def __init__(self):
        self.reader = SimpleMFRC522()
        self.exit = False

        self.queue_data = ""
        self.write_request = False

    def read(self) -> (int, str):
        current_id: int = -1

        try:
            while not self.exit:
                if self.write_request:
                    self.reader.write_no_block(self.queue_data)
                    self.queue_data = ""
                    self.write_request = False
                else:
                    read_id, read_data = None, None
                    for _ in range(3):
                        raw_id, raw_data = self.reader.read_no_block()
                        if raw_id is not None:
                            read_id, read_data = raw_id, raw_data
                        sleep(0.1)

                    if read_id is None:
                        current_id: int = -1
                        yield RfidReader.STATUS_NONE, ""
                    elif read_id is not None:
                        status = (current_id == read_id) + RfidReader.STATUS_NEW
                        current_id = read_id
                        yield status, read_data

        except KeyboardInterrupt:
            self.close()

    def write(self, data: str):
        self.queue_data = data
        self.write_request = True

    def close(self):
        self.exit = True
        GPIO.cleanup()

